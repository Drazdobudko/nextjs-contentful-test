export const getConfig = (environment, preview) => {
    let accessToken = "";
    if (environment === "test") {
        accessToken = process.env.TEST_CONTENTFUL_ACCESS_KEY;
    } else {
        accessToken = process.env.CONTENTFUL_ACCESS_KEY;
    }

    return {
        accessToken,
        spaceId: process.env.CONTENTFUL_SPACE_ID,
        environment,
    }
};

module.export = {
    getConfig: getConfig
}